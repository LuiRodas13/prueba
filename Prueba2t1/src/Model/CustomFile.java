
package Model;


public class CustomFile {
     private int ProductID;
     private String ProductName;
     private double Price;

    public CustomFile() {
    }

    public CustomFile(int ProductID, String ProductName, double Price) {
        this.ProductID = ProductID;
        this.ProductName = ProductName;
        this.Price = Price;
    }

    public int getProductID() {
        return ProductID;
    }

    public void setProductID(int ProductID) {
        this.ProductID = ProductID;
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String ProductName) {
        this.ProductName = ProductName;
    }

    public double getPrice() {
        return Price;
    }

    public void setPrice(double Price) {
        this.Price = Price;
    }
     
     
    
}
