/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import Model.CustomFile;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import views.MainFrame;

/**
 *
 * @author Sistemas-30
 */
public class MainControler implements ActionListener{
    
    private MainFrame frame;
    private JFileChooser fc; 

    public MainControler(MainFrame aThis) {
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
     
    }
    
    public MainControler(){
        frame = new MainFrame();
        fc = new JFileChooser();
    }
    
    
    private File showOpenFileDialog(){
        File file = null;
        if (fc.showOpenDialog(frame)==JFileChooser.APPROVE_OPTION){
            file = fc.getSelectedFile();
            if(file!=null){
                try {
                    readFile(file);
                } catch (IOException ex) {
                    Logger.getLogger(MainControler.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(MainControler.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            else{
               JOptionPane.showMessageDialog(frame, "No se seleccionó archivo");
            }
        }
        return file;
    }
    
         private void showFileInForm(CustomFile customfile)
    {
            frame.add(frame);
            frame.setVisible(true);
           
    }
    
     private void readFile(File file) throws FileNotFoundException, IOException, ClassNotFoundException {
        
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
            CustomFile myCustomFile = (CustomFile) ois.readObject();
            myCustomFile.setProductName(file.getName());
            //JOptionPane.showMessageDialog(frame, myCustomFile.getContent());
            showFileInForm(myCustomFile);
        
    }

  
}
